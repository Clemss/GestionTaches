from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.forms import ModelForm
from django.urls import reverse
from django import forms
from .models import Task

# Create your views here.

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def home(request,name):
    if not name:
        name = "toi"
    return HttpResponse("Hello "+name)

def task_listing(request):
     objects = Task.objects.all().order_by('-name')
     return render(request,'lesTaches/list.html', {"taches": objects })

class Task_form(ModelForm):
    class Meta:
        model=Task
        fields=("name","description","closed")

def task_creation(request):
    if request.method == 'POST':
        form = Task_form(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('LesTaches'))
    else:
        task_form = Task_form()
        return render(request,'lesTaches/tache.html',{'form' : task_form, 'name' : 'Créer une tâche'})

def task_delete(request,name):
    task = Task.objects.filter(name = name)
    task.delete()
    return HttpResponseRedirect(reverse('LesTaches'))

def task_modif(request,name):
    task_form = Task.objects.get(name = name)
    if request.method == 'POST':
        task_form = Task_form(request.POST,instance=task_form)
        if task_form.is_valid():
            task_form.save()
            return HttpResponseRedirect(reverse('LesTaches'))
    else:
        task_form = Task_form(instance=task_form)
        return render(request,'lesTaches/tache.html',{"form":task_form, 'name' : 'Modifier une tâche'})
